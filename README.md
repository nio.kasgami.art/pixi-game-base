# Pixi.js Base Game

A base game developed by inc0der and Nio Kasgami for use with a potential RPG game engine
and future game projects.

## Development

- Clone the repository
  
```sh
git clone https://gitlab.com/LTNGames/pixi-game-base
```

- Install dependencies

```sh
npm install
```

- Run the development script to build and watch for changes

```sh
npm run dev
```

- Test the game

To tes the game your best options is to use vscode or any IDE with a builtin nwjs launch/debugger.

We do have the proper `launch.json` in this repository so if you open up vscode and hit the debug play button, the game will run for you.

## Contribute
Read the [Contribution guide](./CONTRIBUTING.md)

## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
Currently under the [MIT license](./LICENSE)